$(document).ready(function(){
    $("#subscription-form").on("submit", function(e){
        let form = $(this);

        e.preventDefault();
        if (form.valid()){
            $.ajax({
                url: form.attr("action"),
                beforeSend: function() {
                }
            })
            .done(function( data ) {
                if (data.success){
                    form.addClass("form--success");
                    $("#subscription-success-email").text(data.email);
                }
            });
        }
    });
});