/* eslint-disable no-unused-vars */

import '../styles/main.scss'

import $ from '../../node_modules/jquery';
import "../../node_modules/bootstrap/js/src/tab.js";
import "../../node_modules/bootstrap/js/src/dropdown.js";
import "../../node_modules/bootstrap/js/src/collapse.js";
import "../../node_modules/bootstrap-material-design/js/text";
import "../../node_modules/bootstrap-material-design/js/textarea";
import "../../node_modules/bootstrap-material-design/js/autofill";
import "../../node_modules/bootstrap-material-design/js/bootstrapMaterialDesign";
import svg4everybody from "../../node_modules/svg4everybody/dist/svg4everybody.min.js";
import baron from '../../node_modules/baron';
//import '../../node_modules/tablesorter/dist/js/jquery.tablesorter.min.js';

import Utilities from "./utilities.js";
import Drawer from "./drawer.js";
import Authorization from "./authorization.js";
import ArticleVoting from "./article-voting.js";
import InvestBlock from "./invest-block.js";
import ContactForm from "./contact-form.js";

import "./modal.js";
import "./dropdown.js";
import "./form-validation.js";
import "./subscription.js";

if (process.env.NODE_ENV !== 'production') {
  require('../index.pug')
}

$(function(){
    let globalVars = {
      authDeferred: $.Deferred()
    }

    $.tablesorter = tablesorter;

    $('body').bootstrapMaterialDesign({
        global: {
          validate: false,
          label: {
            className: "bmd-label-static" // default style of label to be used if not specified in the html markup
          }
        },
        autofill: {
          selector: "body"
        },
        checkbox: false,
        checkboxInline: false,
        collapseInline: false,
        drawer: false,
        file: false,
        radio: false,
        radioInline: false,
        ripples: false,
        select: false,
        switch: false,
        text: {
          // omit inputs we have specialized components to handle - we need to match text, email, etc.  The easiest way to do this appears to be just omit the ones we don't want to match and let the rest fall through to this.
          selector: [
            `input:not([type=hidden]):not([type=checkbox]):not([type=radio]):not([type=file]):not([type=button]):not([type=submit]):not([type=reset])`
          ]
        },
        textarea: {
          selector: ["textarea"]
        },
        arrive: true,
        // create an ordered component list for instantiation
        instantiation: [
          "ripples",
          "checkbox",
          "checkboxInline",
          "collapseInline",
          "drawer",
          //'file',
          "radio",
          "radioInline",
          "switch",
          "text",
          "textarea",
          "select",
          "autofill"
        ]
    });
    
    svg4everybody();
    Utilities.scrollToLinkInit();
    Utilities.textareaExpandInit();
    Utilities.fileInputInit();

    let articleFilter = new Drawer($("#filter"), [$("#filter-trigger")], false, "filterOpened"),
        menu = new Drawer($("#menu"), [$("#menu-trigger")], false, "menuOpened"),
        authorization = new Authorization(globalVars);

    $("#filter-trigger").on("click", function(){
        if (menu.isActive) menu.hide();
    });

    $("#menu-trigger").on("click", function(){
        if (articleFilter.isActive) articleFilter.hide();
    });

    $(".baron").each(function(i, item){
      baron({
          root: item,
          scroller: $(item).find('.baron__scroller')[0],
          bar: $(item).find('.baron__bar')[0]
      });
    });

    $(".country-dropdown__menu-inner").on('scroll touchmove mousewheel', function(e, d){
        e.stopPropagation();
        if ((d>0 && $(this).scrollTop() === 0) 
                || (d<0 && $(this).scrollTop() === $(this)[0].scrollHeight - $(this).innerHeight())){
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });

    if ($(".article-voting").length){
      let articleVoting = new ArticleVoting(globalVars);
    }

    if ($(".invest-block").length){
      let investBlock = new InvestBlock(globalVars);
    }

    if ($("#contact-form").length){
      let contactForm = new ContactForm(globalVars);
    }

    if ($(".rating-table").length){
      $(".rating-table")
        .tablesorter({
          sortList: [
            [0, 0]
          ],
        })

        .on("sortEnd",function(e, table) {
          let activeColumn = $(table).find("td").not(".tablesorter-headerUnSorted").data("column") + 1;
          $(table).find("td.text-danger").removeClass("text-danger");
          $(table).find("td:nth-child(" + activeColumn + ")").addClass("text-danger");
        });
    }
});

/* eslint-disable no-unused-vars */