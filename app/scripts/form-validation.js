$(document).ready(function(){
    let validateForms = $(".form-validate");

    validator();

    $.extend($.validator.messages, {
        required: "Заполните обязательное поле",
        email: "Введите правильный e-mail",
        url: "Это значение должно начинаться с \"http://\" or \"https://\"",
        date: "Введите верную дату",
        number: "Здесь должно быть число",
        creditcard: "Введите правильный номер карты",
        equal: "Значения не совпадают",
    });

    $.validator.addMethod("email", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/.test(value);
    });


    $.validator.addMethod("equal", function (value, element) {
      var equalTo = $($(element).data("equalto"));
      if (equalTo.not(".validate-equalTo-blur").length) {
        equalTo.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
          $(element).valid();
        });
      }
      return value === equalTo.val();
    });

    validateForms.each(function () {
      let form = $(this),
          id = $(this).attr("id");

      //validate

        form.validate({
            errorClass: "has-danger",
            highlight: function (element, errorClass, validClass) {
              element = $(element);
              var elementParent = element.parents(".form-group").length ? element.parents(".form-group") : element.parent();
              if (element[0].type === "radio") {
                $(element).addClass(errorClass).removeClass(validClass);
              } else {
                element.addClass(errorClass).removeClass(validClass);
                elementParent.addClass(errorClass);
              }
            },
            errorPlacement: function (error, element) {
              if (!element.is("[data-noerror]")) {
                var elementParent = element.parents(".form-group").length ? element.parents(".form-group") : element.parent();
                elementParent.append(error);
              }
            },
            unhighlight: function (element, errorClass, validClass) {
              element = $(element);
              if (element[0].type === "radio") {
                $(element).removeClass(errorClass).addClass(validClass);
              } else {
                element.removeClass(errorClass).addClass(validClass);
                element.parents("." + errorClass).removeClass(errorClass);
              }

              // Unhighlight parent block instead error placement
              if (element.is("[data-highlight]")) {
                var highlightedBlock = $(element.data("highlight"));
                highlightedBlock.removeClass("error-highlight");
              }
            }
        });
    });
});