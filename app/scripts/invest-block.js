import Utilities from "./utilities.js";

export default class InvestBlock {
    constructor(globalVars) {
        this.globalVars = globalVars;
        this.form = $("#invest-form");

        let that = this;

        that.form.on("submit", function(e){
            e.preventDefault(); 
            if (that.form.valid()){
                if (Utilities.isUserAuthorized(globalVars)){
                    that.invest(that.form);
                } else{
                    $("[data-show='auth']").click();
                    that.globalVars.authDeferred.promise().done(function(value) {
                        that.invest(that.form);
                    });
                }
            }
        });

        $("#investment-amount").on("click", function(e){
            e.preventDefault();
            that.reset($(this).data("reset-url"));
        })
    }

    invest(){
        let that = this;

        Utilities.sendAjaxForm(that.form)
            .done(function(data){
                that.form.addClass("form--invested");
                $("#investment-amount").text(Utilities.formatNumber(data.investmentAmount));
                that.form.find("input").val("");
            })
            .fail(function(){
                console.log(error);
            });
    }

    reset(url){
        let that = this;

        $.ajax({
            url: url,
            beforeSend: function() {}
        })
        .done(function(data) {
            $("#investment-amount").text("");
            that.form.removeClass("form--invested");
        })
    }
}