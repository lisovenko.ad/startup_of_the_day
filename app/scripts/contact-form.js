import Utilities from "./utilities.js";

export default class ContactForm {
    constructor(globalVars) {
        this.globalVars = globalVars;
        this.form = $("#contact-form");

        let that = this;

        that.form.on("submit", function(e){
            e.preventDefault(); 
            if (that.form.valid()){
                Utilities.sendAjaxForm(that.form)
                    .done(function(data){
                        that.form.addClass("form--success");
                    })
                    .fail(function(){
                        console.log(error);
                    });
            }
        });
    }
}