import $ from '../../node_modules/jquery';
import '../../node_modules/jquery-mousewheel';

export default class Drawer {
    constructor(element, triggers, isActive, bodyClass) {
        this.element = element;
        this.triggers = triggers;
        this.isActive = this.isActive;
        this.bodyClass = bodyClass;
        this.overlay = $('<div id="overlay" class="overlay"></div>');
        this.scrollableEl = this.element.find(".drawer__scrollable")

        let that = this,
        scrollbarWidth = that.scrollableEl[0].offsetWidth - that.scrollableEl[0].clientWidth;

        $.each(this.triggers, function(index,trigger){
            trigger.on("click", function(){
                if (that.isActive)
                    that.hide();
                else
                    that.show();
            });
        });

        if (scrollbarWidth > 0){
            that.scrollableEl.css({"padding-right" : scrollbarWidth + "px"})
        }

        that.scrollableEl.on('scroll touchmove mousewheel', function(e, d){
            if ((d>0 && that.scrollableEl.scrollTop() === 0) 
                    || (d<0 && that.scrollableEl.scrollTop() === that.scrollableEl[0].scrollHeight - that.scrollableEl.innerHeight())){
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
        });

        $('.modal').on('show.bs.modal', function (e) {
            if (that.isActive) that.hide();
        });

        // $(document).on("scroll touchmove mousewheel", function(e){
        //     if (document.documentElement.offsetWidth <= 1280){
        //         let visibleHeaderHeight = $(".header").height() - document.documentElement.scrollTop > 0 ? $(".header").height() - document.documentElement.scrollTop : 0;

        //         if (visibleHeaderHeight > 0) {
        //             that.element.find(".filter__content").css({"padding-top": visibleHeaderHeight + "px"});
        //         } else {

        //         }
        //     }
        // });
    }

    _createOverlay(){
        let that = this;

        $('body').append(this.overlay);
        this.overlay.on("click", function(){
            that.hide();
        });

        this.overlay.on("scroll touchmove mousewheel", function(e){
            e.preventDefault();
            e.stopPropagation();
            return false;       
        });
    }

    _removeOverlay(){
        this.overlay.remove();
    }

    show(){
        $('.modal:visible').modal("hide");

        this.scrollableEl[0].scrollTop = 0;
        $("body").addClass(this.bodyClass);
        this.isActive = true;
        this._createOverlay();        
        this.element.addClass("active");
    }

    hide(){
        this.element.removeClass("active");
        $("body").removeClass(this.bodyClass);
        this.isActive = false;
        this._removeOverlay();
    }
};