$(document).ready(function(){
    $('.dropdown').on('shown.bs.dropdown', function (e) {
        if($(this)[0].querySelector(".baron__scroller")){
            $(this)[0].querySelector(".baron__scroller").scrollTop=0;
        } else {
            $(this)[0].querySelector(".dropdown-menu").scrollTop=0;
        }
    });
});