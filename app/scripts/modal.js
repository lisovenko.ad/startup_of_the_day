import Modal from "../../node_modules/bootstrap/js/src/modal.js";

$(document).ready(function(){
    let authModal = new Modal($(".modal")[0]);

    $('.modal').on('show.bs.modal', function (e) {
        $(".header__nav").css({"paddingRight" : authModal._getScrollbarWidth() + "px"});
    });

    $('.modal').on('hidden.bs.modal', function (e) {
        $(".header__nav")[0].removeAttribute("style");

        if ($(this).find(".form--success").length){
            $(this).find(".form--success").removeClass("form--success");
        }
    });
});