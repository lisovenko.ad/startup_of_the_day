import Utilities from "./utilities.js";

export default class Autorization {
	constructor(globalVars) {
		this.globalVars = globalVars;
		let that = this;

		$(".auth-link").on("click", function(){
			$('#authorizationTabs a[href="#' +$(this).data("show") + '"]').tab('show');
			$("#authorization").modal("show");
		});

		$(".authorization .back").on("click", function(e){
			e.preventDefault();
			$("[data-show=auth").click();
		});

		$(".registraion-link").on("click", function(e){
			e.preventDefault();
			$("[data-show=reg").click();
		});

		$("#authorization-form").on("submit", function(e){
			e.preventDefault();

			let form = $(this); 

			if (form.valid()){
				Utilities.sendAjaxForm(form)
					.done(function(data){
						that.globalVars.authDeferred.resolve(data.success);
						//that.globalVars.isAuthorized = true;
						$("#authorization").modal("hide");
					})
					.fail(function(){
						that.globalVars.authDeferred.reject();
						console.log(error);
					});
			}
		});

		$("#registration-form").on("submit", function(e){
			e.preventDefault();

			let form = $(this); 

			if (form.valid()){
				Utilities.sendAjaxForm(form)
					.done(function(data){
						form.addClass("form--success");
						$("#registration-success-email").text(data.email);
					})
					.fail(function(){
						console.log(error);
					});
			}
		});
	}
}