import Utilities from "./utilities.js";

export default class ArticleVoting {
    constructor(globalVars) {
        this.globalVars = globalVars;

        let that = this;

        $("[data-is-successful]:not(.voted)").on("click", function(){
            let isSuccessful = $(this).data("isSuccessful"),
                btn = $(this)

            if (Utilities.isUserAuthorized(globalVars)){
                that.vote(btn, isSuccessful);
            } else{
                $("[data-show='auth']").click();
                that.globalVars.authDeferred.promise().done(function(value) {
                    that.vote(btn, isSuccessful);
                });
            }
        });
    }

    vote(btn, isSuccessful){
        $.ajax({
            url: $(".article-voting").data("action"),
            data: JSON.stringify({ isSuccessful: isSuccessful }),
            beforeSend: function() {}
        })
        .done(function(data) {
            $("[data-is-successful]").removeClass("voted");
            btn.addClass("voted");
        })
        .fail(function(){
        });
    }
}