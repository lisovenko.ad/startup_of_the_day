import $ from '../../node_modules/jquery';

function isUserAuthorized(globalVars){
    return globalVars.authDeferred.promise().state() == "resolved";
}

function sendAjaxForm(form){
    let ajaxDeferred = $.Deferred();
    
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        beforeSend: function() {}
    })
    .done(function(data) {
        ajaxDeferred.resolve(data);
    })
    .fail(function(){
        ajaxDeferred.reject();
    });
    return ajaxDeferred.promise();
}

function scrollToLinkInit(){
    $(".scrollTo-link").on("click", function(e){
        e.preventDefault();
        let target  = $(this).attr("href");
        if ($(target).length) {
          $('html, body').animate({'scrollTop': $(target).offset().top - $(".header").height()}, 500);
        }
    });
}

function textareaExpandInit(){
    $(document)
    .one('focus.autoExpand', 'textarea.autoExpand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows,
            rowHeight = parseFloat($(this).css("line-height"));

        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / rowHeight);
        this.rows = minRows + rows;
    });
}

function fileInputInit(){
    $('.file-input [type="file"]').change(function(){
        var label = $(this).siblings(".file-input__label");
        console.log(label)
        if(typeof(this.files) !='undefined'){
            if(this.files.length == 0){
                label.removeClass('withFile').text(label.data('default'));
            }
            else{

                var file = this.files[0]; 
                var name = file.name;
                var size = (file.size / 1048576).toFixed(3); //size in mb 
                label.addClass('withFile').text(name);
            }
        }
        else{
            var name = this.value.split("\\");
            label.addClass('withFile').text(name[name.length-1]);
        }
        return false;
    });
}

function formatNumber(x) {
    var parts = x.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    return parts.join('.');
}

module.exports = {
    isUserAuthorized: isUserAuthorized,
    sendAjaxForm: sendAjaxForm,
    scrollToLinkInit: scrollToLinkInit,
    formatNumber: formatNumber,
    textareaExpandInit: textareaExpandInit,
    fileInputInit: fileInputInit
};