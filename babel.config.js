module.exports = api => {
  const isProd = api.cache(() => process.env.NODE_ENV === 'production')

  return {
    plugins: [
      '@babel/plugin-syntax-dynamic-import',
      ["@babel/plugin-proposal-object-rest-spread", { "loose": true, "useBuiltIns": true }],
      '@babel/plugin-syntax-object-rest-spread',
      '@babel/plugin-transform-object-assign'
    ],
    presets: [
      [
        '@babel/preset-env',
        {
          //modules: false,
          targets: {
            esmodules: !isProd,
            browsers: ["last 2 versions", "safari >= 7", "IE 11"]
          }
        }
      ]
    ]
  }
}
